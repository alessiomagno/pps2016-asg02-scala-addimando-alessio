package gameView;

import gameModel.objects.Coin;
import gameModel.objects.GameObject;

import java.util.List;

/**
 * Created by Alessio Addimando on 15/03/2017.
 */
public interface LevelStrategy {

    List<GameObject> positionObject();

    List<Coin> positionCoin();
}

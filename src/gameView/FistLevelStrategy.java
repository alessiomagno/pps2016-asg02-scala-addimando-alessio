package gameView;

import gameModel.GameElementConcreteFactory;
import gameModel.GameElementFactory;
import gameModel.objects.Coin;
import gameModel.objects.GameObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessio Addimando on 15/03/2017.
 */
public class FistLevelStrategy implements LevelStrategy {

    private final GameElementFactory factory = new GameElementConcreteFactory();

    @Override
    public List<GameObject> positionObject() {

        List<GameObject> objects = new ArrayList<>();
    
        objects.add(factory.createTunnel(600, 230));
        objects.add(factory.createTunnel(1000, 230));
        objects.add(factory.createTunnel(1600, 230));
        objects.add(factory.createTunnel(1900, 230));
        objects.add(factory.createTunnel(2500, 230));
        objects.add(factory.createTunnel(3000, 230));
        objects.add(factory.createTunnel(3800, 230));
        objects.add(factory.createTunnel(4500, 230));

        objects.add(factory.createBlock(400, 180));
        objects.add(factory.createBlock(1200, 180));
        objects.add(factory.createBlock(1270, 170));
        objects.add(factory.createBlock(1340, 160));
        objects.add(factory.createBlock(1800, 180));
        objects.add(factory.createBlock(2600, 160));
        objects.add(factory.createBlock(2650, 180));
        objects.add(factory.createBlock(3500, 160));
        objects.add(factory.createBlock(3550, 140));
        objects.add(factory.createBlock(4150, 180));
        objects.add(factory.createBlock(4250,145 ));
        objects.add(factory.createBlock(4350, 110));
        
        return objects;
    }

    @Override
    public List<Coin> positionCoin() {
        
        List<Coin> coins = new ArrayList<>();
        coins.add(factory.createCoin(402, 145));
        coins.add(factory.createCoin(1202, 140));
        coins.add(factory.createCoin(1272, 95));
        coins.add(factory.createCoin(1342, 40));
        coins.add(factory.createCoin(1650, 145));
        coins.add(factory.createCoin(2650, 145));
        coins.add(factory.createCoin(3000, 135));
        coins.add(factory.createCoin(3400, 125));
        coins.add(factory.createCoin(4200, 15));
        coins.add(factory.createCoin(4600, 40));

        return coins;
    }



}

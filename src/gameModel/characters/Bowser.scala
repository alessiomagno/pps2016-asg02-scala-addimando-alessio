package gameModel.characters

import java.awt.Image

import gameModel.characters.Bowser.{HEIGHT, WIDTH}
import utilities.{ResourceConstant, ResourcesGetter}

/**
  * Created by Alessio Addimando on 08/04/2017.
  */
object Bowser {
  val WIDTH: Int = 110
  val HEIGHT: Int = 140

  def apply(x: Int, y: Int): Bowser = new BowserImpl(x, y)

  private class BowserImpl ( x : Int , y : Int ) extends Bowser(x,y) {
    assert( x != null && y != null )
  }
}

class Bowser(x: Int, y: Int) extends EnemyCharacter(x, y, WIDTH, HEIGHT) with Runnable {

  val bowserThread: Thread = new Thread(this)
  bowserThread start()

  override def move(speed : Int) {
    setX(getX - speed )
  }

  def getImage(name: String): Image = ResourcesGetter getImage name

  override def getImageOfDeath: Image = ResourcesGetter getImage ResourceConstant.IMG_BOWSER

}
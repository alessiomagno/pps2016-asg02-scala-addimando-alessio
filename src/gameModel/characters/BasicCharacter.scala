package gameModel.characters

import gameModel.GameElement
import gameModel.RealGameElement
import utilities.ResourceConstant
import utilities.ResourcesGetter
import java.awt._

object BasicCharacter {
  val PROXIMITY_MARGIN: Int = 10
  val OBJECT_GAP: Int = 5
}

abstract class BasicCharacter(x: Int, y: Int, val width: Int, val height: Int) extends RealGameElement(x, y, width, height) with Character {
  private var moving: Boolean = false
  private var toRight: Boolean = true
  private var counter: Int = 0
  private var alive: Boolean = true
  private var walkingFrameOn: Boolean = true

  def isAlive: Boolean = alive

  def isToRight: Boolean = toRight

  def getAlive: Boolean = alive

  def setAlive(value: Boolean): Unit = alive = value

  def setMoving(value: Boolean): Unit = moving = value

  def setToRight(value: Boolean): Unit = toRight = value

  def hitAhead(component: GameElement): Boolean = {
    !(getX + getWidth < component.getX || getX + getWidth > component.getX + BasicCharacter.OBJECT_GAP || getY + getWidth <= component.getY || getY >= component.getY + component.getHeight) }

  def hitBack(`object`: GameElement): Boolean = {
    !(getX > (`object`.getX + `object`.getWidth) || getX + getWidth < `object`.getX + `object`.getWidth - BasicCharacter.OBJECT_GAP || getY + getHeight <= `object`.getY || getY >= `object`.getY + `object`.getHeight) }


  def hitBelow(`object`: GameElement): Boolean = {
    !(getX + getWidth < `object`.getX + BasicCharacter.OBJECT_GAP || getX > `object`.getX + `object`.getWidth - BasicCharacter.OBJECT_GAP || getY + getHeight < `object`.getY || getY + getHeight > `object`.getY + BasicCharacter.OBJECT_GAP) }

  def hitAbove(component: GameElement): Boolean = {
    !(getX + getWidth < component.getX + BasicCharacter.OBJECT_GAP || getX > component.getX + component.getWidth - BasicCharacter.OBJECT_GAP || getY < component.getY + component.getHeight || getY > component.getY + component.getHeight + BasicCharacter.OBJECT_GAP) }

  def isNearToComponent(component: GameElement): Boolean = {
    getX > component.getX - BasicCharacter.PROXIMITY_MARGIN && getX < component.getX + component.getWidth + BasicCharacter.PROXIMITY_MARGIN || (getX + getWidth > component.getX - BasicCharacter.PROXIMITY_MARGIN && getX + getWidth < component.getX + component.getWidth + BasicCharacter.PROXIMITY_MARGIN) }

  def walkingImage(name: String, frequency: Int): Image = {
    if ( {
      this.counter += 1; this.counter
    } % frequency == 0) {
      walkingFrameOn = !walkingFrameOn
    }
    val str: String = ResourceConstant.IMG_BASE concat  name concat (if (!this.moving || walkingFrameOn) ResourceConstant.IMG_STATUS_ACTIVE
    else ResourceConstant.IMG_STATUS_NORMAL) concat (if (this.toRight) ResourceConstant.IMG_DIRECTION_DX
    else ResourceConstant.IMG_DIRECTION_SX) concat  ResourceConstant.IMG_EXT
     ResourcesGetter getImage str
  }

  def contactWithOtherComponent(component: GameElement)
}
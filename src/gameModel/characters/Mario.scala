package gameModel.characters

import gameController.GameLoopController
import gameModel.GameElement
import gameModel.objects.Block
import gameModel.objects.Coin
import utilities.ResourceConstant
import utilities.ResourcesGetter
import java.awt._

import utilities.ResourceConstant.IMG_GOLDEN_BLOCK
import utilities.ResourceConstant.IMG_MARIO_SUPER_DX


object Mario {
   val MARIO_OFFSET_Y_INITIAL: Int = 245
   val MARIO_OFFSET_X_INITIAL: Int = 300
   val FLOOR_OFFSET_Y_INITIAL: Int = 293
   val WIDTH: Int = 28
   val HEIGHT: Int = 50
   val JUMPING_LIMIT: Int = 42
   val JUMPING_SPEED: Int = 4
}

class Mario() extends BasicCharacter(Mario.MARIO_OFFSET_X_INITIAL, Mario.MARIO_OFFSET_Y_INITIAL, Mario.WIDTH, Mario.HEIGHT) {
  private var jumping: Boolean = false
  private var jumpingExtent: Int = 0

  def isJumping: Boolean = jumping

  def setJumping(value: Boolean) = jumping = value


  def doJump: Image = {
    var str: String = null
    if ( {
      jumpingExtent += 1
      jumpingExtent
    } < Mario.JUMPING_LIMIT) {
      if (getY > GameLoopController.getController.getScene.getHeightLimit) setY(getY - Mario.JUMPING_SPEED)
      else {
        jumpingExtent = Mario.JUMPING_LIMIT
      }
      str = if (isToRight) IMG_MARIO_SUPER_DX
      else ResourceConstant.IMG_MARIO_SUPER_SX
    }
    else if (getY + getHeight < GameLoopController.getController.getScene.getDistanceVerticalFromFloor) {
      setY(getY + 1)
      str = if (isToRight) IMG_MARIO_SUPER_DX
      else ResourceConstant.IMG_MARIO_SUPER_SX
    }
    else {
      str = if (isToRight) ResourceConstant.IMG_MARIO_ACTIVE_DX
      else ResourceConstant.IMG_MARIO_ACTIVE_SX
      jumping = false
      jumpingExtent = 0
    }
    ResourcesGetter.getImage(str)
  }

  def contactWithOtherComponent(component: GameElement) {
    component match {
      case enemy: Bowser =>
        if (hitAhead(enemy) || hitBack(enemy) || hitAbove(enemy) || hitBelow(enemy)) setAlive(false)
      case enemy: EnemyCharacter =>
        if (hitAhead(component) || hitBack(component)) {
          if (enemy.isAlive) {
            setMoving(false)
            setAlive(false)
          }
          else {
            setAlive(true)
          }
        }
        else if (hitBelow(component)) {
          enemy.setMoving(false)
          enemy.setAlive(false)
        }
      case _ =>
        if (hitAhead(component) && isToRight || hitBack(component) && !isToRight) {
          setMoving(false)
          GameLoopController.getController.getScene.setMov(0)
        }
        if (hitBelow(component) && jumping) {
          GameLoopController.getController.getScene.setDistanceVerticalFromFloor(component.getY)
        }
        else if (!hitBelow(component)) {
          GameLoopController.getController.getScene.setDistanceVerticalFromFloor(Mario.FLOOR_OFFSET_Y_INITIAL)
          if (hitAbove(component)) {
            component match {
              case block: Block =>
                block.setImage(ResourcesGetter.getImage(IMG_GOLDEN_BLOCK))
            }
            GameLoopController.getController.getScene.setHeightLimit(component.getY + component.getHeight)
          }
          else if (!hitAbove(component) && !jumping) {
            GameLoopController.getController.getScene.setHeightLimit(0)
          }
          if (!jumping && getY < ResourceConstant.FLOOR) { falling()
          }

        }
        def falling() = {
          jumping = true
          jumpingExtent = Mario.JUMPING_LIMIT
        }

    }

  }

  def contactCoin(coin: Coin): Boolean = hitBack(coin) || hitAbove(coin) || hitAhead(coin) || hitBelow(coin)
}

package gameModel.characters

import utilities.ResourceConstant
import utilities.ResourcesGetter
import java.awt._

import gameModel.characters.Turtle.{HEIGHT, WIDTH}

object Turtle {
  val WIDTH: Int = 43
  val HEIGHT: Int = 50

  def apply(x: Int, y: Int): Turtle = new TurtleImpl(x, y)

  private class TurtleImpl ( x : Int , y : Int ) extends Turtle(x,y) {
    assert( x != null && y != null )
  }
}

class Turtle(x: Int, y: Int) extends EnemyCharacter(x, y, WIDTH, HEIGHT) {
  val turtleThread: Thread = new Thread(this)
  turtleThread start()

  def getImageOfDeath: Image = ResourcesGetter.getImage(ResourceConstant.IMG_TURTLE_DEAD)

}
package gameModel.characters

import gameModel.GameElement
import java.awt.Image

/**
  * this interface represents the behaviour of a gameController's character
  */
trait Character {
  def isAlive: Boolean

  def isToRight: Boolean

  def setAlive(alive: Boolean)

  def setMoving(moving: Boolean)

  def setToRight(toRight: Boolean)

  def walkingImage(name: String, frequency: Int): Image

  def isNearToComponent(component: GameElement): Boolean

  def contactWithOtherComponent(component: GameElement)

  def move(speed : Int)
}
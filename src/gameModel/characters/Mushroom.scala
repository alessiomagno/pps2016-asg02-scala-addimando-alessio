package gameModel.characters

import utilities.ResourceConstant
import utilities.ResourcesGetter
import java.awt._

import gameModel.characters.Mushroom.{HEIGHT, WIDTH}


object Mushroom {
  val WIDTH: Int = 15
  val HEIGHT: Int = 30

  def apply(x: Int, y: Int): Mushroom = new MushroomImpl(x, y)

  private class MushroomImpl ( x : Int , y : Int ) extends Mushroom(x,y) {
    assert( x != null && y != null )
  }
}
 class Mushroom(x: Int, y: Int) extends EnemyCharacter(x, y, WIDTH, HEIGHT) with Runnable {
  val mushroomThread: Thread = new Thread(this)
  mushroomThread start()

  def getImageOfDeath: Image = {
    ResourcesGetter.getImage(
      if (isToRight) ResourceConstant.IMG_MUSHROOM_DEAD_DX
      else ResourceConstant.IMG_MUSHROOM_DEAD_SX
    )

  }
}
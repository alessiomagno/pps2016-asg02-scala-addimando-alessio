package gameModel.characters

import java.awt.Image
import gameModel.characters.Twomp.{FLOOR_MAX, FLOOR_MIN, HEIGHT, WIDTH}
import utilities.{ResourceConstant, ResourcesGetter}

/**
  * Created by Alessio Addimando on 08/04/2017.
  */
object Twomp {
  val WIDTH: Int = 30
  val HEIGHT: Int = 50
  val FLOOR_MAX: Int = 25
  val FLOOR_MIN: Int = 243

  def apply(x: Int, y: Int): Twomp = new TwompImpl(x, y)

  private class TwompImpl ( x : Int , y : Int ) extends Twomp(x,y) {
    assert( x != null && y != null )
  }
}

class Twomp(x: Int, y: Int) extends EnemyCharacter(x, y, WIDTH, HEIGHT) {
  private var ascending: Boolean = true
  val twompThread: Thread = new Thread(this)
  twompThread start()

  override def move(speed : Int) {
   if(getY < FLOOR_MAX) ascending = false
   if(getY > FLOOR_MIN) ascending = true
    setX(getX - speed )
    if(ascending) setY(getY - 1)
    else setY(getY + 3)

  }

  def getImage(name: String): Image = ResourcesGetter getImage name

  override def getImageOfDeath: Image = ResourcesGetter getImage ResourceConstant.IMG_TWOMP

}
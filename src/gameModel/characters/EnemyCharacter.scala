package gameModel.characters

import gameModel.GameElement
import java.awt._
import java.lang.Thread._

/**
  * Created by Alessio Addimando on 08/04/2017.
  */
abstract class EnemyCharacter(x: Int, y: Int, width: Int, height: Int) extends BasicCharacter(x, y, width, height) with Runnable {

  val PAUSE: Int = 25
  private var offsetX: Int = 1
  super.setToRight(true)
  super.setMoving(true)

  override def move(speed : Int) {
    offsetX = if(isAlive){ if (isToRight) 1 else -1} else 0
    setX(getX + offsetX - speed)
  }

  def run() {
    while (isAlive) try {
        sleep(PAUSE)
      }
      catch {
        case e: Throwable =>  System.out.println(e.getStackTrace)
      }
  }

  def contactWithOtherComponent(component: GameElement) {
    if (hitAhead(component) && isToRight) {
      setToRight(false)
      offsetX = -1
    }
    else if (hitBack(component) && !isToRight) {
      setToRight(true)
      offsetX = 1
    }
  }

  def changeImageWhenDie: Image = getImageOfDeath

  def getImageOfDeath: Image
}
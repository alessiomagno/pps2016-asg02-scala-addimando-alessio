package gameModel

import gameController.GameLoopController

/**
  * Created by Alessio Addimando on 08/04/2017.
  */
class RealGameElement(var x: Int, var y: Int, width: Int, height: Int) extends GameElement {

  def getX: Int = x

  def getY: Int =  y

  def getWidth: Int = width

  def getHeight: Int = height

  def setX(value: Int):Unit = x = value

  def setY(value: Int):Unit = y = value

  def move(speed: Int) {
    if (GameLoopController.getController.getScene.getHorizontalPosition >= 0) {
      x -= GameLoopController.getController.getScene.getMov
    }
  }
}
package gameModel

/**
  * Created by Alessio Addimando on 08/04/2017.
  */
trait GameElement {

  def setX(x: Int)

  def setY(y: Int)

  def getX: Int

  def getY: Int

  def getWidth: Int

  def getHeight: Int

  def move(speed : Int)
}
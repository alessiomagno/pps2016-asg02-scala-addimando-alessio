package gameModel

import gameModel.characters._
import gameModel.objects.Block
import gameModel.objects.Coin
import gameModel.objects.Tunnel

/**
  * Created by Alessio Addimando on 08/04/2017.
  */
trait GameElementFactory {

  def createMushroom(x: Int, y: Int): Mushroom

  def createTurtle(x: Int, y: Int): Turtle

  def createTunnel(x: Int, y: Int): Tunnel

  def createCoin(x: Int, y: Int): Coin

  def createBlock(x: Int, y: Int): Block

  def createBowser(x: Int, y: Int): Bowser

  def createTwomp (x: Int, y: Int): Twomp
}
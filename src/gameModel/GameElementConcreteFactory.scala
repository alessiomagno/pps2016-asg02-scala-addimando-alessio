package gameModel

import gameModel.characters.{Bowser, Mushroom, Turtle, Twomp}
import gameModel.objects.Block
import gameModel.objects.Coin
import gameModel.objects.Tunnel

/**
  * Created by Alessio Addimando on 08/04/2017.
*/

class GameElementConcreteFactory extends GameElementFactory {

  def createMushroom(x: Int, y: Int): Mushroom = Mushroom(x, y)


  def createTurtle(x: Int, y: Int): Turtle = Turtle(x, y)


  def createTunnel(x: Int, y: Int): Tunnel = Tunnel(x, y)


  def createCoin(x: Int, y: Int): Coin = Coin(x, y)


  def createBlock(x: Int, y: Int): Block = Block(x, y)

  override def createBowser(x: Int, y: Int): Bowser = Bowser(x,y)

  override def createTwomp(x: Int, y: Int): Twomp = Twomp(x,y)
}
package gameModel.objects

import gameModel.GameElement
import java.awt._

/**
  * Created by Alessio Addimando on 08/04/2017.
  */
trait GameObject extends GameElement {

  def getImage: Image

  def setImage(image: Image)
}
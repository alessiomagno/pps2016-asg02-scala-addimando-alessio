package gameModel.objects

import gameModel.objects.Tunnel.{HEIGHT, WIDTH}
import utilities.ResourceConstant
import utilities.ResourcesGetter

object Tunnel {
  val WIDTH: Int = 43
  val HEIGHT: Int = 65

  def apply(x: Int, y: Int): Tunnel = new TunnelImpl(x, y)

  private class TunnelImpl (x : Int ,y : Int ) extends Tunnel(x,y) {
    assert( x != null && y != null )
  }
}

class Tunnel(x: Int,y: Int) extends RealGameObject(x, y, WIDTH, HEIGHT) {
  super.setImage(ResourcesGetter.getImage(ResourceConstant.IMG_TUNNEL))
}
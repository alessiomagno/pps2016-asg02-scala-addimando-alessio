package gameModel.objects

import gameModel.objects.Block.{HEIGHT, WIDTH}
import utilities.ResourceConstant
import utilities.ResourcesGetter

object Block {
  val WIDTH: Int = 30
  val HEIGHT: Int = 30

  def apply(x: Int, y: Int): Block = new BlockImpl(x, y)

  private class BlockImpl ( x : Int , y : Int ) extends Block(x,y) {
    assert( x != null && y != null )
  }
}

class Block(x: Int, y: Int) extends RealGameObject(x, y, WIDTH, HEIGHT) {
  super.setImage(ResourcesGetter getImage ResourceConstant.IMG_BLOCK)
}
package gameModel.objects

import java.awt.Image

import gameModel.characters.EnemyCharacter
import utilities.{ResourceConstant, ResourcesGetter}

import scala.collection.mutable.ListBuffer

/**
  * Created by Alessio Addimando on 09/04/2017.
  */


class PlantManager {

  private val plantsCollection = ListBuffer[Plant]()

  plantsCollection += StaticPlant(ResourcesGetter.getImage(ResourceConstant.IMG_RPLANT))
  plantsCollection += DynamicPlant(ResourcesGetter.getImage(ResourceConstant.IMG_YPLANT), ResourcesGetter.getImage(ResourceConstant.IMG_BPLANT))


  def getStaticPlantImage(): Image = {
    val static: StaticPlant = plantsCollection.filter(x => x.isInstanceOf[StaticPlant]).asInstanceOf[ListBuffer[StaticPlant]].head
    static.image
  }

  def getDynamicPlantImage(indexImage: Int): Image= {
    val dynamic: DynamicPlant =  plantsCollection.filter(x => x.isInstanceOf[DynamicPlant]).asInstanceOf[ListBuffer[DynamicPlant]].head
    indexImage match {
      case 1 => dynamic.imageBefore
      case 2 => dynamic.imageAfter
    }
  }
}
  abstract class Plant

  case class StaticPlant(image: Image) extends Plant{
    def getImage() : Image = image
  }

  case class DynamicPlant(imageBefore: Image, imageAfter: Image) extends Plant{
    def getImageBefore() : Image = imageBefore
    def getImageAfter() : Image = imageAfter

  }




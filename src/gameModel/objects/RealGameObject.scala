package gameModel.objects

import gameModel.RealGameElement
import java.awt._

class RealGameObject(x: Int, y: Int, width: Int, height: Int) extends RealGameElement(x, y, width, height) with GameObject {
  private var image: Image = null

  def getImage: Image = {
    return this.image
  }

  def setImage(image: Image) {
    this.image = image
  }
}
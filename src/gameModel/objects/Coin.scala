package gameModel.objects

import utilities.ResourceConstant
import utilities.ResourcesGetter
import java.awt._

object Coin {
  val WIDTH: Int = 30
  val HEIGHT: Int = 30
  private val PAUSE: Int = 10
  val FLIP_FREQUENCY: Int = 100

  def apply(x: Int, y: Int): Coin = new CoinImpl(x, y)

  private class CoinImpl ( x : Int , y : Int ) extends Coin(x,y) {
    assert( x != null && y != null )
  }
}

class Coin(x: Int, y: Int) extends RealGameObject(x, y, Coin.WIDTH, Coin.HEIGHT) with Runnable {
  setImage(ResourcesGetter.getImage(ResourceConstant.IMG_PIECE1))
  private var counter: Int = 0
  private var coinShowed: Boolean = true

  def imageOnMovement: Image = {
    if ( {
      counter += 1; counter
    } % Coin.FLIP_FREQUENCY == 0) {
      coinShowed = !coinShowed
    }
    ResourcesGetter.getImage(
      if (coinShowed) ResourceConstant.IMG_PIECE1
    else ResourceConstant.IMG_PIECE2)
  }

  def run() {
    while (true) {
      imageOnMovement
      try {
        Thread.sleep(Coin.PAUSE)
      }
      catch {
        case e: InterruptedException => System.out.println(e.getStackTrace)
      }
    }
  }
}
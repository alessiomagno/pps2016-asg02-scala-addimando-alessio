package gameController;

import gameView.Platform;
import utilities.ResourceConstant;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static utilities.ResourceConstant.SCENARIO_LENGTH;

public class Keyboard implements KeyListener {


    Platform scene;
    @Override
    public void keyPressed(KeyEvent e) {

        scene = GameLoopController.getController().getScene();
        if (scene.getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                if (scene.getHorizontalPosition() == -1) {
                    scene.setHorizontalPosition(0);
                    scene.setBackground1HorizontalPosition(-50);
                    scene.setBackground2HorizontalPosition(750);
                }
                if(scene.getMov() == 0) {
                    scene.getMario().setMoving(true);
                }
                scene.getMario().setToRight(true);
                scene.setMov(1);
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (scene.getHorizontalPosition() == 4601) {
                    scene.setHorizontalPosition(SCENARIO_LENGTH);
                    scene.setBackground1HorizontalPosition(-50);
                    scene.setBackground2HorizontalPosition(750);
                }

                scene.getMario().setMoving(true);
                scene.getMario().setToRight(false);
                scene.setMov(-1);
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                scene.getMario().setJumping(true);
                AudioPlayer.playSound(ResourceConstant.AUDIO_JUMP);
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        scene.getMario().setMoving(false);
        scene.setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
